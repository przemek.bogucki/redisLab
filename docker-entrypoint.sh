#!/bin/bash

echo check: $IP $PORT $BPORT

cp /etc/redis-cluster-template.conf redis-cluster.conf

EXIT=false

if  [[ "$IP" == "" ]]; then
    echo "IP environment variable for 'cluster-announce-ip address' is obligatory"
    EXIT=true
fi
if [[ "$PORT" == "" ]]; then
    echo "PORT environment for 'announce-port' is obligatory"
    EXIT=true
fi
if [[ "$BPORT" == "" ]]; then
    echo "BPORT environment for 'announce-bus-port' is obligatory"
    EXIT=true
fi

if [[ "$EXIT" == "true"  ]]; then
	exit 1
fi

sed -i 's/{{announce-port}}/'$PORT'/g'  redis-cluster.conf
sed -i 's/{{announce-ip}}/'$IP'/g'  redis-cluster.conf
sed -i 's/{{announce-bus-port}}/'$BPORT'/g'  redis-cluster.conf

if [[ "$DUMPSEC" != "" && "$DUMPRECORDS" != "" ]]; then
	sed -i 's/{{dump-sec}}/'$DUMPSEC'/g'  redis-cluster.conf       
	sed -i 's/{{dump-records}}/'$DUMPRECORDS'/g'  redis-cluster.conf  
else 
	sed -i 's/save {{dump-sec}} {{dump-records}}/#save 60 5000/g'  redis-cluster.conf       
fi


# allow the container to be started with `--user`
exec redis-server redis-cluster.conf --protected-mode no
