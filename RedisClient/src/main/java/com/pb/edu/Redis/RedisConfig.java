package com.pb.edu.Redis;

import com.pb.edu.ApplicationConfiguration;
import io.lettuce.core.ReadFrom;
import io.lettuce.core.SocketOptions;
import io.lettuce.core.TimeoutOptions;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.ClusterTopologyRefreshOptions;
import io.lettuce.core.resource.ClientResources;
import io.lettuce.core.resource.DefaultClientResources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

@Lazy
@Configuration
public class RedisConfig {

    @Autowired
    ApplicationConfiguration applicationConfiguration;

    @Lazy
    @Bean
    public StringRedisTemplate getKeyValueRedisTemplate() {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setConnectionFactory(getRedisConnectionFactory());
        return redisTemplate;
    }

    @Bean
    @Lazy
    public LettuceConnectionFactory getRedisConnectionFactory() {
        if (applicationConfiguration.isAdvancedClient()) {
            return new LettuceConnectionFactory(getRedisClusterConfiguration(), getRedisClientConfiguration());
        } else {
            return new LettuceConnectionFactory(getRedisClusterConfiguration());
        }
    }

    public RedisClusterConfiguration getRedisClusterConfiguration() {
        RedisClusterConfiguration configuration = new RedisClusterConfiguration(applicationConfiguration.getHosts());
        configuration.setMaxRedirects(3);
        return configuration;
    }

    public LettuceClientConfiguration getRedisClientConfiguration() {
        final ClientResources clientResources = DefaultClientResources.builder()
                .ioThreadPoolSize(4)
                .computationThreadPoolSize(4)
                .build();

        final SocketOptions socketOptions = SocketOptions.builder()
                .keepAlive(true)
                .tcpNoDelay(true)
                .connectTimeout(Duration.ofSeconds(5))
                .build();

        final ClusterTopologyRefreshOptions topologyRefreshOptions = ClusterTopologyRefreshOptions.builder()
                .enablePeriodicRefresh(Duration.ofSeconds(10))
                .dynamicRefreshSources(true)
                .enableAllAdaptiveRefreshTriggers()
                .closeStaleConnections(true)
                .enableAllAdaptiveRefreshTriggers()
                .adaptiveRefreshTriggersTimeout(Duration.ofSeconds(3))
                .build();

        final TimeoutOptions timeoutOptions = TimeoutOptions.builder().
                timeoutCommands(true).
                fixedTimeout(Duration.ofSeconds(5)).build();

        final ClusterClientOptions clusterClientOptions = ClusterClientOptions.builder()
                .socketOptions(socketOptions)
                .topologyRefreshOptions(topologyRefreshOptions)
                .timeoutOptions(timeoutOptions)
                .autoReconnect(true)
                .build();

        return LettuceClientConfiguration.builder().readFrom(ReadFrom.ANY).clientOptions(clusterClientOptions).clientResources(clientResources).build();
    }
}
