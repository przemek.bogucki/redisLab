package com.pb.edu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;

@Component
@ConfigurationProperties("conf")
public class ApplicationConfiguration {

    @Value("${conf.sleepTimeInSec}")
    private int sleepTimeInSec = 1000;
    @Value("${conf.advancedClient}")
    private boolean advancedClient = true;
    @Value("${conf.hosts}")
    private Collection<String> hosts = Arrays.asList("127.0.0.1:7001");
    @Value("${conf.iterations}")
    private int iterations = 0;

    @Autowired
    private ApplicationArguments applicationArguments;

    @Bean
    public ApplicationConfiguration getApplicationConfiguration() {
        update(applicationArguments.getSourceArgs());
        return this;
    }

    private void update(String[] args) {
        try {
            if (args.length > 0) {
                sleepTimeInSec = Integer.parseInt(args[0]);
            }
            if (args.length > 1) {
                advancedClient = Boolean.parseBoolean(args[1]);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Incorrect input parameters: " + Arrays.toString(args));
        }
    }

    public static String getConfigurationHint() {
        return "Expected push interval (int)";
    }


    public boolean isAdvancedClient() {
        return advancedClient;
    }

    public int getSleepTimeInSec() {
        return sleepTimeInSec;
    }

    public Collection<String> getHosts() {
        return hosts;
    }

    public int getIterations() {
        return iterations;
    }
}
